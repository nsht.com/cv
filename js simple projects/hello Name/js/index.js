
var fullDate = new Date();
var hours = fullDate.getHours();

let inputIn = document.getElementById('userName');
let text = document.getElementById('text');
let button = document.querySelector('button');

button.onclick = ()=>{
    if(inputIn.value){
        time();
    }else{
        text.innerHTML = 'Hello!';
    }
}

const time = () => {
    if (hours<=12 && hours>=6){
            text.innerHTML = `Good morning, ${inputIn.value}!`;
    }else if(hours<=17 && hours > 12){
        text.innerHTML = `Good day, ${inputIn.value}!`;
    }else if(hours<=21 && hours > 17){
        text.innerHTML = `Good evening, ${inputIn.value}!`;
    }else{
        text.innerHTML = `Good night, ${inputIn.value}!`;
    }
}
