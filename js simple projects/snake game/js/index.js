//змійка
const N = 11 // розмір поля 11Х11
let snakeHtml = document.getElementById('snake')
let snake = [
    [5,5],
    [6,5],
    [7,5],
] //масив координат змійки
let apple = false;
let lastMove = 'up';
let canMove = true;

function checkApple(){
    if(apple){
        draw();
    }else{
        getApple();
    }
}

function getApple(){
    let x = Math.floor( Math.random() * N);
    let y = Math.floor( Math.random() * N);
    let isFind = false;
    for(let s = 0; s < snake.length; s++){
        if(snake[s][0] == y && snake[s][1] == x){
            isFind = true;
            break;
        }
    }
    if(isFind){
        getApple();
    }else{
        apple = [];
        apple[0]= y;
        apple[1]= x;
        draw();
    }
}

function draw(){
    let html = '';
    let className = '';
    for(let y = 0; y < N; y++){
        html += '<div class="row">';
            for(let x = 0; x < N; x++){
                className = '';
                for(let s = 0; s < snake.length; s++){
                    if(y == snake[s][0] && x == snake[s][1]){
                        className = ' snake ';
                        break;
                    }
                }
                if(apple[0] == y && apple[1] == x){
                    className += ' apple '
                }
                html += '<div class="box '+ className +'" x="'+x+'" y="'+y+'"></div>';
            }
        html += '</div>'
    }
    snakeHtml.innerHTML = html;
    setTimeout(step, 400);
}
function cloneItem(item){
    let newItem = [];
    newItem[0]=item[0]; // скопіювали х
    newItem[1]=item[1]; // скопіювали у
    return newItem;
}
function next(index = 0, type = 0){ // 1 - рух не до нуля
    if(!canMove) return;
    let nextStep = cloneItem(snake[0]);
    if(type){
        nextStep[index]=(nextStep[index]-1 < 0) ? N-1 : --nextStep[index];
    }else{
        nextStep[index]=(nextStep[index]+1 >= N) ? 0 : ++nextStep[index];
    }

    let itsMe = false;
    for(let s = 0; s < snake.length; s++){
        if(nextStep[0] == snake[s][0] && nextStep[1] == snake[s][1]){
            itsMe = true;
            canMove = false;
            alert ('STOP');
            break;
        }
    }

    snake.unshift(nextStep);

    let isApple = false;
    if(nextStep[0] == apple[0] && nextStep[1] == apple[1]){
        isApple = true;
    }

    if(!isApple){
        snake.pop();
    }else{
        apple = false;
    }
    checkApple();
}

// function up(){
//     next(0,1)
//     /*
//     let nextStep = cloneItem(snake[0]) //ми не можем змінювати нульову координату, нам треба створити нову
//     // коли ми копіюєм якісь значення масиви то js памятає в якій адресі памяті воно знаходилось
//     //тому нам потрібне клонування
//     nextStep[0]=(nextStep[0]-1 < 0) ? N-1 : --nextStep[0]
//     snake.unshift(nextStep); //додає елемент на початку масиву
//     snake.pop(); //видаляє останній елемент масиву
//     // for(let s = 0; s < snake.length; s++){
//     //     snake[s][0] = (snake[s][0]-1 < 0) ? N-1 : --snake[s][0]
//     //     // if(snake[s][0]-1 < 0){
//     //     //     snake[s][0]= N-1;
//     //     // }else{
//     //     //     snake[s][0]--;
//     //     // }
//     // }
//     draw()
//     */
// }
// function down(){
//     next(0,0);
//     /*
//     let nextStep = cloneItem(snake[0])
//     nextStep[0]=(nextStep[0]+1 >= N) ? 0 : ++nextStep[0]
//     snake.unshift(nextStep);
//     snake.pop();
//     draw()
//     */
// }
//
//
// function right(){
//     next(1,0);
//     /*
//     let nextStep = cloneItem(snake[0])
//     nextStep[1]=(nextStep[1]+1 >= N) ? 0 : ++nextStep[1]
//     snake.unshift(nextStep);
//     snake.pop();
//     draw()
//     */
// }
//
// function left(){
//     next(1,1);
//     /*
//     let nextStep = cloneItem(snake[0])
//     nextStep[1]=(nextStep[1]-1 < 0) ? N-1 : --nextStep[1]
//     snake.unshift(nextStep);
//     snake.pop();
//     draw()
//     */
// }

function step(){
    switch (lastMove) {
        case 'up': next(0,1); break;
        case 'down': next(0,0); break;
        case 'right': next(1,0); break;
        case 'left': next(1,1); break;
        default:

    }
}

document.addEventListener('keyup', function(event){
    if(event.keyCode == 38 && lastMove !== 'down'){
        lastMove = 'up';
        // up();
    }else if(event.keyCode == 40 && lastMove !== 'up'){
        lastMove = 'down';
        // down();
    }else if(event.keyCode == 39 && lastMove !== 'left'){
        lastMove = 'right';
        // right();
    }else if(event.keyCode == 37 && lastMove !== 'right'){
        lastMove = 'left';
        // left();
    }
});
checkApple();


// docoment це обєкт веб сторінки, який має багато методів
// є такі події mouseover, mouseout, click, keyup, keydown, keypres
// якщо функція немає імені, вона називається анонімною
// document.getElementById('P1').addEventListener('mouseover', m1);
// document.getElementById('P1').addEventListener('mouseout', m2);
//
// function m1(){
//     console.log('11111');
// }
// function m2(){
//     console.log('222222');
// }

//тернарний оператор
/*
let x = 0;
let y = 5;

if(x>0){
    y=1;
}else{
    y=0;
}

y = (x>0) ? 1 : 0; //може записуватись тільки одна дія вираз

function a(h){
    let x =;
    ////
    ////
    ////
    return (x>0) ? 1 : 0;
}
a((x>0) ? 1 : 0);
//тернарний оператор використовується коли треба щось  повернути, передати...
//якщо до циклів чи умовних операторів відноситься один рядок, то скобки {} можна не писати
*/
