//menue

$('.menuIcon').click(function(){
    let isVisible = $('.navs').is(':visible');
    if(isVisible){
        $('.navs').slideUp();
    }else{
        $('.navs').slideDown();
    }
})


$('#carousel1').owlCarousel({
    loop:true,
    nav:true,
    dots: false,
    margin: 30,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }
})
$('#carousel2').owlCarousel({
    loop:true,
    nav:false,
    dots: true,
    margin: 30,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})
function openCity(evt, cityName) {
      var i, img, tablinks;

      img = document.getElementsByClassName("img");
      for (i = 0; i < img.length; i++) {
        img[i].style.display = "none";
      }

      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace("active", "");
      }

      special=document.getElementsByClassName(cityName);
      for (i = 0; i < special.length; i++) {
        special[i].style.display = "inline-block";
      }
      evt.currentTarget.className += " active";
}

//при кліке на ссилку сторінка не перезагружається
$('.tab a').on('click', function (e) { e.preventDefault() })
